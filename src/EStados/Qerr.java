/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EStados;

import principal.Contexto;

/**
 *
 * @author Jorge
 */
public class Qerr implements Estados{

    @Override
    public void transitar(Contexto contexto, char simbolo) {
        contexto.setEstado(new Qerr());
    }

    @Override
    public boolean esFinal() {
        return false;
    }
    
}
