/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EStados;

import principal.Contexto;

/**
 *
 * @author Jorge
 */
public class Q2 implements Estados{

    @Override
    public void transitar(Contexto contexto, char simbolo) {
        if(Character.isDigit(simbolo)){
            contexto.setEstado(new Q2());
        }else{
            contexto.setEstado(new Qerr());
        }
    }

    @Override
    public boolean esFinal() {
        return true;
    }
    
}
