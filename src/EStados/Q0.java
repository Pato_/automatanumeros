/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EStados;

import principal.Contexto;

/**
 *
 * @author Jorge
 */
public class Q0 implements Estados{

    @Override
    public void transitar(Contexto contexto, char simbolo) {
        if(simbolo == '+' || simbolo == '-'){
            contexto.setEstado(new Q1());
        }else if(Character.isDigit(simbolo)){
            contexto.setEstado(new Q2());
        }
        
    }

    @Override
    public boolean esFinal() {
        return false;
    }
    
}
