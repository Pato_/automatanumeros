/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import EStados.Q0;

/**
 *
 * @author Jorge
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String palabra = "+123";
        char[] partes = palabra.toCharArray();
        Contexto con = new Contexto();
        con.setEstado(new Q0());
        for(char c:partes){
            con.getEstado().transitar(con, c);
        }
        if(con.getEstado().esFinal()) System.out.println("Es correcto");
        else System.out.println("No es correcto");
        
    }
    
}
